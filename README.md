# Главная страница проекта

### Инструкция по установке

Скачивание и установка:
```
git clone https://gitlab.com/Sergei-Smirnov-95/team_3.git
cd team_3
python3 setup.py install
```

Запуск:
```
sudo monitor
```

Программа запускает web-сервер, с которого по **GET**-запросу выдаёт всю информацию об окружающей среде в формате **JSON**.
