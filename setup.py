from setuptools import setup

setup(
    name='monitor',
    version='1.0',
    description='This is monitor of temperature,humidity,vibration and pressure parameters.',
    author='team_3',
    packages=['src'],
    url = "https://gitlab.com/Sergei-Smirnov-95/team_3",
    install_requires=[
      'Adafruit_DHT',
      'Adafruit_CharLCD',
      'Adafruit_BMP',
      'mcp3208'
    ],
    entry_points = {
    'console_scripts' : ['monitor = src.__main__:run']
    }
    )
