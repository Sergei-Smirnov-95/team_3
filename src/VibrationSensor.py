from .Sensor import Sensor
from .ADC import ADC

class VibrationSensor(Sensor):

    type = "%"

    def _read(self):
        return ADC.read(self.port)

    @classmethod
    def _from_voltage_to_value(cls, volts):
        return int(volts / 5)

