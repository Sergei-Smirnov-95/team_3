from .Sensor import Sensor
from .ADC import ADC


class TemperatureSensor(Sensor):

    type = "°C"

    def _read(self):
        return ADC.read(self.port)

    @classmethod
    def _from_voltage_to_value(cls, volts):
        """
        По милливольту на градус Цельсия, начиная с нуля.
        3 а не 3.3 так как стоит стабилизатор
        """
        return round(((volts/10)*1000/4096)*3,1)
