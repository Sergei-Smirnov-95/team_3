from abc import ABC, abstractmethod


class Sensor(ABC):

    type = None

    def __init__(self, port):
        self.port = port

    @abstractmethod
    def _read(self):
        """
        Считывает показания в вольтах с датчика.
        """

    @classmethod
    @abstractmethod
    def _from_voltage_to_value(cls, volts):
        """
        Определяет соответствие значению датчика и значению в единицах измерения для этого же датчика.
        """
        pass

    def get_meas(self):
        """
        Возвращает текущее значения для сенсора.
        """
        return self._from_voltage_to_value(self._read())
