import Adafruit_BMP.BMP085 as BMP085

from .Sensor import Sensor


class PressureSensor(Sensor):

    type = "Pa"

    def __init__(self):
        self.sensor = BMP085.BMP085()
            
    def _read(self):
        return self.sensor.read_pressure()

    @classmethod
    def _from_voltage_to_value(cls, volts):
        return volts

