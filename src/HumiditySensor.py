import Adafruit_DHT

from .Sensor import Sensor


class HumiditySensor(Sensor):

    type = "%"

    def _read(self):
        humidity, _  = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, self.port)
        return humidity


    @classmethod
    def _from_voltage_to_value(cls, volts):
        return round(volts, 1)


